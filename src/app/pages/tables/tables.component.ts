import { Component, OnInit } from '@angular/core';
import { FournisseurService } from 'src/app/services/fournisseur.service';
import { Router } from '@angular/router';
import { Fournisseur } from 'src/app/models/Fournisseur';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  fournisseurs: Observable<Fournisseur[]>;

  constructor(private _fournisseurService:FournisseurService, private route:Router ) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.fournisseurs = this._fournisseurService.getFournisseursList();
  
  
  }

  deleteFournisseur(idFournisseur:any) {
    this._fournisseurService.deleteFournisseur(idFournisseur)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

}
