export class Category {
    id: any;
    desig: string;
    subCategories: SubCategory[];
}

export class SubCategory {
    id: any;
    name: string;
}
