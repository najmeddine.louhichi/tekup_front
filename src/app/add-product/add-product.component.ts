import { Component, OnInit } from '@angular/core';
import { Product } from '../models/Product';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  product: Product = new Product();
  submitted = false;
  constructor(private _productService: ProductService, private router: Router) { }

  ngOnInit(): void {
  }
newProduct(): void {
  this.submitted = false;
  this.product = new Product();

}

save() {
  this._productService.createProduct(this.product)
    .subscribe(data => console.log(data), error => console.log(error));
  this.product = new Product();
  this.gotoList();
}

onSubmit() {
  this.submitted = true;
  this.save();
}

gotoList() {
  this.router.navigate(['/products']);
}

}
