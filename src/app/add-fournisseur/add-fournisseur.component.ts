import { Component, OnInit } from '@angular/core';
import { Fournisseur } from '../models/Fournisseur';
import { FournisseurService } from '../services/fournisseur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-fournisseur',
  templateUrl: './add-fournisseur.component.html',
  styleUrls: ['./add-fournisseur.component.css']
})
export class AddFournisseurComponent implements OnInit {
  fournisseur: Fournisseur
  = new Fournisseur();
  submitted = false;
  constructor(private _fournisseurService:FournisseurService, private router :Router) { }

  ngOnInit(): void {
  }
  newFournisseur(): void{
    this.submitted = false;
    this.fournisseur = new Fournisseur();
  
  }

  save() {
    this._fournisseurService.createFournisseur(this.fournisseur)
      .subscribe(data => console.log(data), error => console.log(error));
    this.fournisseur = new Fournisseur();
    this.gotoList();
  }
  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/tables']);
  }
}
