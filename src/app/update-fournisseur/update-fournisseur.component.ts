import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FournisseurService } from '../services/fournisseur.service';
import { Fournisseur } from '../models/Fournisseur';
@Component({
  selector: 'app-update-fournisseur',
  templateUrl: './update-fournisseur.component.html',
  styleUrls: ['./update-fournisseur.component.css']
})
export class UpdateFournisseurComponent implements OnInit {
  idFournisseur: any;
  fournisseur: Fournisseur;
  constructor(private route: ActivatedRoute,private router: Router,
    private _fournisseurService: FournisseurService) { }

  ngOnInit(): void {
    

    this.fournisseur = new Fournisseur();
    this.idFournisseur = this.route.snapshot.params['idFournisseur'];
    this._fournisseurService.getFournisseur(this.idFournisseur)
    .subscribe(data => {
      console.log(data)
      this.fournisseur = data;
    }, error => console.log(error));

  }
  updateFournisseur() {
    this._fournisseurService.updateFournisseur(this.idFournisseur, this.fournisseur)
      .subscribe(data => console.log(data), error => console.log(error));
    this.fournisseur = new Fournisseur();
    this.gotoList();
  }

  onSubmit() {
    this.updateFournisseur();    
  }

  gotoList() {
    this.router.navigate(['/tables']);
  }

}
