import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private baseUrl = 'http://localhost:9095/category/';
  constructor(private http: HttpClient) {

  }


  getCategory(idCat: any): Observable<any> {
    return this.http.get(`${this.baseUrl + 'id'}/${idCat}`);
  }
  createCategory(category: Object): Observable<Object> {
    console.log(category);
    return this.http.post(`${this.baseUrl + 'add'}`, category);
  }
  updateCategory(idCat: any, value: any): Observable<Object> {
    console.log(idCat);
    return this.http.put(`${this.baseUrl + 'update'}/${idCat}`, value);
  }
  deleteCategory(idCat: any): Observable<any> {
    return this.http.delete(`${this.baseUrl + 'delete'}/${idCat}`, { responseType: 'text' });
  }
  getCategorysList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }


}
