import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'http://localhost:9095/user/';
  constructor(private http: HttpClient) { }

  getUser(idUser: any): Observable<any> {
    return this.http.get(`${this.baseUrl+"id"}/${idUser}`);
  }
  createUser(user: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl+"add"}`, user);
  }
  updateUser(idUser: any, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl+"update"}/${idUser}`, value);
  }
  deleteUser(idUser: any): Observable<any> {
    return this.http.delete(`${this.baseUrl+"delete"}/${idUser}`, { responseType: 'text' });
  }
  getUsersList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

}

