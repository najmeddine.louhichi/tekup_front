import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService{

  private baseUrl = 'http://localhost:9095/product/';

  constructor(private http: HttpClient) { }

  getProduct(id: any): Observable<any> {
    return this.http.get(`${this.baseUrl+"/id"}/${id}`);
  }

  createProduct(product: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl+"add"}`, product);
  }

  updateProduct(id: any, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl+"update"}/${id}`, value);
  }

  deleteProduct(id: any): Observable<any> {
    return this.http.delete(`${this.baseUrl+"delete"}/${id}`, { responseType: 'text' });
  }

  getProductsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

}

