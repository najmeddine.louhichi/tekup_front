import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FournisseurService {
  private baseUrl = 'http://localhost:9095/fournisseur/';
  constructor(private http: HttpClient) { }

  getFournisseur(idFournisseur: any): Observable<any> {
    return this.http.get(`${this.baseUrl+"idFournisseur"}/${idFournisseur}`);
  }
  createFournisseur(fournisseur: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl+"add"}`, fournisseur);
  }
  updateFournisseur(idFournisseur: any, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl+"update"}/${idFournisseur}`, value);
  }
  deleteFournisseur(idFournisseur: any): Observable<any> {
    return this.http.delete(`${this.baseUrl+"delete"}/${idFournisseur}`, { responseType: 'text' });
  }
  getFournisseursList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

}
