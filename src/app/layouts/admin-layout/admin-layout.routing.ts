import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { ProductsComponent } from '../../pages/icons/products.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { AddProductComponent } from 'src/app/add-product/add-product.component';
import { UpdateProductComponent } from 'src/app/update-product/update-product.component';
import { AddFournisseurComponent } from 'src/app/add-fournisseur/add-fournisseur.component';
import { UpdateFournisseurComponent } from 'src/app/update-fournisseur/update-fournisseur.component';
import { AddUserComponent } from 'src/app/add-user/add-user.component';
import { UpdateUserComponent} from 'src/app/update-user/update-user.component';
import { CategoryBrandComponent} from 'src/app/category-brand/category-brand.component';
import { AddCategoryComponent } from 'src/app/add-category/add-category.component';
import { UpdateCategoryComponent} from 'src/app/update-category/update-category.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: ProductsComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'providers',         component: TablesComponent },
    { path: 'products',          component: ProductsComponent },
    { path: 'addProduct',     component: AddProductComponent },
    { path: 'updateProduct/:id',  component: UpdateProductComponent },
    { path: 'addProvider',     component: AddFournisseurComponent },
    { path: 'updateProvider/:idFournisseur',   component: UpdateFournisseurComponent },
    { path: 'addUser',     component: AddUserComponent },
    { path: 'updateUser/:idUser',   component: UpdateUserComponent },
    { path: 'categoryBrand',   component: CategoryBrandComponent },
    { path: 'addCategory',   component: AddCategoryComponent },
    { path: 'updatecategory/:idCat',   component: UpdateCategoryComponent },
];
